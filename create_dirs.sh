#!/usr/bin/env bash
mkdir uploads
mkdir images
mkdir resized_images

chmod -R 777 uploads
chmod -R 777 images
chmod -R 777 resized_images