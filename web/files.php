<?php

require_once 'process_image.php';

// Some settings
$data = array();
$error = false;
$files = array();
$upload_dir = './uploads/';
$images_dir = './images/';

// Get previous uploaded content
$dir_files = scandir($upload_dir);
$previous_content = [];

foreach ($dir_files as $file) {
    if (!is_dir($file)) {
        $previous_content = array_merge($previous_content, file($upload_dir . $file));
    }
}

$resize_images = [];

// Process uploaded by dropzone files
foreach ($_FILES as $file) {
    $new_file = $upload_dir . "_" . time() . "_" . basename($file['name']);
    if (move_uploaded_file($file['tmp_name'], $new_file)) {

        // Get diff for download new files
        $new_content = file($new_file);
        $diff = array_diff($new_content, $previous_content);

        // Download all new files by diff
        foreach ($diff as $url) {
            $url = rtrim($url);
            if (filter_var($url, FILTER_VALIDATE_URL)) {
                $remote_content = file_get_contents($url);
                $url_parts = mb_split("/", $url);
                $filename = $url_parts[count($url_parts) - 1];

                $filename = rawurldecode($filename);
                $image_path = $images_dir . $filename;

                file_put_contents($image_path, $remote_content);
                $resize_images[] = process_image($image_path, $filename);
            }
        }

        // This is uploaded by dropzone file
        $files[] = $upload_dir . $file['name'];
    } else {
        $error = true;
    }
}
$data = ($error) ? array('error' => 'There was an error uploading your files') : array('files' => $files, 'images' => $resize_images);

echo json_encode($data);