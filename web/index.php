<?php

$resize_images_dir = "/resized_images/";
$images = scandir("." . $resize_images_dir);

$images_src = [];

foreach ($images as $image) {
    if (!is_dir($image)) {
        $images_src[] = $resize_images_dir . $image;
    }
}

// Require view
require_once 'view/index.php';
