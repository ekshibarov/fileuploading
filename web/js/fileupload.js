$(function() {
    var wall = new freewall("#images-container");
    wall.reset({
        selector: '.item',
        animate: true,
        cellW: 20,
        cellH: 200,
        onResize: function() {
            wall.fitWidth();
        }
    });
    wall.fitWidth();
    // for scroll bar appear;
    $(window).trigger("resize");

    Dropzone.options.imagesDropzone = {
        maxFilesize: 2, // MB
        acceptedFiles: ".txt",
        success: function(file, response) {
            response = JSON.parse(response);
            var images_container = $("#images-container");
            $.each(response.images, function(key, value) {
                var img = document.createElement("img");
                $(img).attr("src", value);
                $(img).attr("alt", value);
                $(img).addClass("pading-5px");
                $(img).addClass("item");
                images_container.append(img);
            });
            wall.fitWidth();
        }
    };
});

