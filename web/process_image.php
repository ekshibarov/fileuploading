<?php

function process_image($path, $filename)
{

    // Scale image
    $image = new \Imagick(realpath($path));
    $image->scaleImage(0, 200);

    // Add watermark
    $watermark = new Imagick();
    $watermark->readImage("./watermark/watermark.png");
    $image->compositeImage($watermark, imagick::COMPOSITE_OVER, 0, 0);

    // Save image
    $result_dir = "./resized_images/";
    $result_image = $result_dir . $filename;
    file_put_contents($result_image, $image->getImageBlob());
    return "/resized_images/{$filename}";
}
